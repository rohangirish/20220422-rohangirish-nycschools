package com.example.nychighschool.NYCSchoolData
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.junit.Assert;

@RunWith(JUnit4::class)
class ValidatorTest{

    @Test
    fun whenInputIsValid(){
        val nycSchoolDataModelApi =  NYCSchoolDataModelApi("test school",
            "test location",
            "test neighbourhood",
            "test phonenumber",
            "test website",
            "test dbn")
            val result = Validator.validateInput(nycSchoolDataModelApi)
            Assert.assertEquals(result,true)
    }

    @Test
    fun whenInputINotValid(){
        val nycSchoolDataModelApi =  NYCSchoolDataModelApi("test school",
            "test location",
            "",
            "test phonenumber",
            "test website",
            "test dbn")
        val result = Validator.validateInput(nycSchoolDataModelApi)
        Assert.assertEquals(result,false)
    }
}