package com.example.nychighschool.SATScores
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ValidatorTest{

    @Test
    fun whenInputIsValid(){
        val satScoreDataModel =  SATScoreDataModel("test school",
            "test location",
            "test neighbourhood",
            "test phonenumber",
            "test website",
            "test dbn")
        val result = Validator.validateInput(satScoreDataModel)
        Assert.assertEquals(result,true)
    }

    @Test
    fun whenInputINotValid(){
        val satScoreDataModel =  SATScoreDataModel("test school",
            "test location",
            "",
            "test phonenumber",
            "test website",
            "test dbn")
        val result = Validator.validateInput(satScoreDataModel)
        Assert.assertEquals(result,false)
    }
}