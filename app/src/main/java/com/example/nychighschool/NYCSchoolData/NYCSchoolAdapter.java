package com.example.nychighschool.NYCSchoolData;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.nychighschool.DetailsActivity;
import com.example.nychighschool.R;

import java.util.ArrayList;

/*Have written the function in java so that i demonstrate both my skills of java and kotlin in same project*/
public class NYCSchoolAdapter extends RecyclerView.Adapter<NYCSchoolAdapter.ViewHolder> {

    //Global variable declaration
    ArrayList<NYCSchoolDataModelApi> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context mainActivity;

    // data is passed into the constructor along with the activity context
    public NYCSchoolAdapter(Context context, ArrayList<NYCSchoolDataModelApi> mData) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = mData;
        mainActivity = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.school_data_row, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NYCSchoolDataModelApi data = mData.get(position);
        //Initializing The text views
        holder.txtSchoolName.setText(data.getSchool_name());
        holder.txtSchoolDBN.setText(data.getDbn());
        holder.txtLocation.setText(data.getLocation());
        holder.txtNeighbourhood.setText(data.getNeighborhood());
        holder.txtPhoneNumber.setText(data.getPhone_number());
        holder.txtWebsite.setText(data.getWebsite());
        //Onlick function to open the next activity passing on the DBN details to fetch SAT Scores
        holder.btnShowDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mainActivity, DetailsActivity.class);
                i.putExtra("DBN",holder.txtSchoolDBN.getText().toString());
                mainActivity.startActivity(i);
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtSchoolName,txtSchoolDBN,txtLocation,txtNeighbourhood,txtPhoneNumber,txtWebsite;
        Button btnShowDetails;

        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            txtSchoolName = itemView.findViewById(R.id.txtSchoolName);
            txtSchoolDBN = itemView.findViewById(R.id.txtSchoolDBN);
            txtLocation = itemView.findViewById(R.id.txtLocation);
            txtNeighbourhood = itemView.findViewById(R.id.txtNeighbourhood);
            txtPhoneNumber = itemView.findViewById(R.id.txtPhoneNumber);
            txtWebsite = itemView.findViewById(R.id.txtWebsite);
            btnShowDetails = itemView.findViewById(R.id.btnShowDetails);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}