package com.example.nychighschool.NYCSchoolData

object Validator {

    fun validateInput(nycSchoolDataModelApi: NYCSchoolDataModelApi): Boolean {
        return (!nycSchoolDataModelApi.getSchool_name().isEmpty()&&
                !nycSchoolDataModelApi.getLocation().isEmpty()&&
                !nycSchoolDataModelApi.getNeighborhood().isEmpty()&&
                !nycSchoolDataModelApi.getPhone_number().isEmpty()&&
                !nycSchoolDataModelApi.getWebsite().isEmpty()&&
                !nycSchoolDataModelApi.getDbn().isEmpty());
    }

}