package com.example.nychighschool.NYCSchoolData;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.nychighschool.MainActivity;

import org.json.JSONArray;

import java.util.ArrayList;

/*Have written the function in java so that i demonstrate both my skills of java and kotlin in same project*/
public class NYCSchoolApiRequest {
    //Baseurl to fetch the api data
    private   static final String BASE_URL="https://data.cityofnewyork.us/resource/s3k6-pzi2.json";
    MainActivity mainActivity;
    RequestQueue queue;

    //Constructor to initialise the variables
    public NYCSchoolApiRequest(MainActivity mainActivity){
        this.mainActivity=mainActivity;
    }

    /*
        Volley is a networking library it offers great features like prioritization, ordered requests and can help making multiple request at the same time
        making multiple requests at the same time like asynchronous and synchronous calls. Volley api calls can be highly customised based on api requirements
     */
    public void apiRequest(){
        queue = Volley.newRequestQueue(mainActivity);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // enjoy your response
                        mainActivity.convertStringToDataModel(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // enjoy your error status
                mainActivity.displayError("Api Error : "+error.getMessage());
            }
        });
        queue.add(stringRequest);
    }
}
