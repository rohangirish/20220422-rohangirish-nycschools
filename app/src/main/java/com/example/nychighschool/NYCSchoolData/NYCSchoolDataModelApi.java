package com.example.nychighschool.NYCSchoolData;

/*Have written the function in java so that i demonstrate both my skills of java and kotlin in same project*/
public class NYCSchoolDataModelApi {

    //DataModel for api response
    public String school_name;
    public String location;
    public String neighborhood;
    public String phone_number;
    public String website;
    public String dbn;

    //Constructor To initialise the model with values
    public NYCSchoolDataModelApi(String school_name, String location, String neighborhood, String phone_number, String website, String dbn) {
        this.school_name = school_name;
        this.location = location;
        this.neighborhood = neighborhood;
        this.phone_number = phone_number;
        this.website = website;
        this.dbn = dbn;
    }

    //Getter and Setter Functions
    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

}
