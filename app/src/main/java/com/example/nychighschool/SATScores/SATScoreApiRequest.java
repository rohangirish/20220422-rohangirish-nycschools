package com.example.nychighschool.SATScores;

import android.content.DialogInterface;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.nychighschool.DetailsActivity;
import com.example.nychighschool.MainActivity;

import org.json.JSONArray;
import org.json.JSONObject;

/*Have written the function in java so that i demonstrate both my skills of java and kotlin in same project*/
public class SATScoreApiRequest {
    private   static final String BASE_URL="https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=";
    DetailsActivity detailsActivity;
    RequestQueue queue;
    SATScoreDataModel satScoreDataModel;

    //Constructor to initialise the variables
    public SATScoreApiRequest(DetailsActivity detailsActivity){
        this.detailsActivity=detailsActivity;
    }

    /*
        Volley is a networking library it offers great features like prioritization, ordered requests and can help making multiple request at the same time
        making multiple requests at the same time like asynchronous and synchronous calls. Volley api calls can be highly customised based on api requirements
     */
    public void apiRequest(String dbn){
        String url = BASE_URL + dbn;
        queue = Volley.newRequestQueue(detailsActivity);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Converting the string response to array and parsing the data and initialising the datamodel class
                        try {
                            JSONArray jsonArray;
                            jsonArray  = new JSONArray(response);
                            JSONObject oneObject = (JSONObject) jsonArray.getJSONObject(0);
                            satScoreDataModel= new SATScoreDataModel(
                                    oneObject.getString("dbn"),
                                    oneObject.getString("school_name"),
                                    oneObject.getString("num_of_sat_test_takers"),
                                    oneObject.getString("sat_critical_reading_avg_score"),
                                    oneObject.getString("sat_math_avg_score"),
                                    oneObject.getString("sat_writing_avg_score")
                            );
                            //Calls the set data method to display data on screen
                            detailsActivity.setData(satScoreDataModel);
                        }
                        catch (Exception e){
                            //Handling Empty response from api
                            detailsActivity.displayError("The Particular college is not register for sat or the api doesn't have the data");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // enjoy your error status
                detailsActivity.displayError("Api Error :"+error.getMessage());
            }
        });
        queue.add(stringRequest);
    }
}
