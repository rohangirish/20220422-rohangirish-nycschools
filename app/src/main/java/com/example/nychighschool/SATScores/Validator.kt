package com.example.nychighschool.SATScores

object Validator {

    fun validateInput(satScoreDataModel: SATScoreDataModel): Boolean {
        return (!satScoreDataModel.getSchool_name().isEmpty()&&
                !satScoreDataModel.getNum_of_sat_test_takers().isEmpty()&&
                !satScoreDataModel.getSat_critical_reading_avg_score().isEmpty()&&
                !satScoreDataModel.getSat_math_avg_score().isEmpty()&&
                !satScoreDataModel.getSat_writing_avg_score().isEmpty()&&
                !satScoreDataModel.getDbn().isEmpty());
    }

}