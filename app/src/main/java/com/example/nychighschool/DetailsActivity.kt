package com.example.nychighschool

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.nychighschool.NYCSchoolData.NYCSchoolApiRequest
import com.example.nychighschool.SATScores.SATScoreApiRequest
import com.example.nychighschool.SATScores.SATScoreDataModel
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception


class DetailsActivity : AppCompatActivity() {
    //Global Variable Declaration
    private var txtDbn: TextView? = null
    private  var txtSchoolName:TextView? = null
    private  var txtNumberTestTakers:TextView? = null
    private  var txtAverageScore:TextView? = null
    private  var txtMathScore:TextView? = null
    private  var txtWritingScore:TextView? = null
    var satScoreDataModel: SATScoreDataModel? = null
    var satScoreApiRequest : SATScoreApiRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Setting the view of the screen from the resource folder
        setContentView(R.layout.activity_details)
        //Calling a function to initialise all global variables with view using their ids
        initialiseViews()
        //Calling function to get Data from api
        getData()
    }

    fun initialiseViews() {
        //Initialising views with respective id from the layout
        txtDbn = findViewById<TextView>(R.id.txtDbn)
        txtSchoolName = findViewById<TextView>(R.id.txtSchoolName)
        txtNumberTestTakers = findViewById<TextView>(R.id.txtNumberTestTakers)
        txtAverageScore = findViewById<TextView>(R.id.txtAverageScore)
        txtMathScore = findViewById<TextView>(R.id.txtMathScore)
        txtWritingScore = findViewById<TextView>(R.id.txtWritingScore)
        satScoreDataModel = SATScoreDataModel()
        satScoreApiRequest = SATScoreApiRequest(this@DetailsActivity)
    }

    fun getData() {
        //Requesting Data from api using Volley and once the data is fetched it calls the setData() function to display data on screen
       satScoreApiRequest?.apiRequest(intent.getStringExtra("DBN")!!)
    }

    //Setting the data to the view from api response
    fun setData(satScoreDataModel:SATScoreDataModel) {
        txtDbn?.setText(satScoreDataModel!!.getDbn())
        txtSchoolName?.setText(satScoreDataModel!!.getSchool_name())
        txtNumberTestTakers?.setText(satScoreDataModel!!.getNum_of_sat_test_takers())
        txtAverageScore?.setText(satScoreDataModel!!.getSat_critical_reading_avg_score())
        txtMathScore?.setText(satScoreDataModel!!.getSat_math_avg_score())
        txtWritingScore?.setText(satScoreDataModel!!.getSat_writing_avg_score())
    }

    //Displaying error message from api as an alert dialog
    fun displayError(errorMessage:String){
        //Handling Empty response from api
        val alertDialog = AlertDialog.Builder(this@DetailsActivity).create()
        alertDialog.setTitle("Alert")
        alertDialog.setMessage(errorMessage)
        alertDialog.setButton(
            AlertDialog.BUTTON_NEUTRAL, "OK"
        ) { dialog, which -> onBackPressed() }
        alertDialog.show()
    }

}