package com.example.nychighschool

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nychighschool.NYCSchoolData.NYCSchoolAdapter
import com.example.nychighschool.NYCSchoolData.NYCSchoolApiRequest
import com.example.nychighschool.NYCSchoolData.NYCSchoolDataModelApi
import org.json.JSONArray
import java.util.ArrayList


class MainActivity : AppCompatActivity() {
    //Global Variable Declaration
    var progressBar: ProgressBar? = null
    var schoolDataView: RecyclerView? = null
    var nycSchoolDataModelApi: ArrayList<NYCSchoolDataModelApi>? = null
    var nycSchoolAdapter: NYCSchoolAdapter? = null
    var nycSchoolApiRequest : NYCSchoolApiRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Setting the view of the screen from the resource folder
        setContentView(R.layout.activity_main)
        //Calling a function to initialise all global variables with view using their ids
        initialiseViews()
        //Calling a function to get Data from api
        getData()
    }

    fun initialiseViews() {
        //Initialising views with respective id from the layout
        nycSchoolApiRequest = NYCSchoolApiRequest(this@MainActivity)
        progressBar = findViewById<ProgressBar>(R.id.progressBar)
        schoolDataView = findViewById<RecyclerView>(R.id.schoolDataView)
        schoolDataView?.layoutManager = LinearLayoutManager(this)
    }

    fun getData() {
        //Requesting Data from api using Volley and once the data is fetched it calls the setData() function to populate data on screen
        nycSchoolApiRequest?.apiRequest()
    }

    fun setData() {
        //Initialise a adapter to populate the api data and display on the recycler view
        nycSchoolAdapter = NYCSchoolAdapter(this@MainActivity, nycSchoolDataModelApi)
        schoolDataView!!.adapter = nycSchoolAdapter
    }

    //Parsing the api response to json array and then respective data model class
    fun convertStringToDataModel(response:String){
        //Converting the string response to JSON Array
        val jsonArray =  JSONArray(response)
        //Initialising the DataModel class with empty constructor
        nycSchoolDataModelApi = ArrayList()
        //Parsing the json array into the ApiDataModel
        for (i in 0 until jsonArray.length()) {
            nycSchoolDataModelApi!!.add(
                NYCSchoolDataModelApi(
                    jsonArray.getJSONObject(i).getString("school_name"),
                    jsonArray.getJSONObject(i).getString("location"),
                    jsonArray.getJSONObject(i).getString("neighborhood"),
                    jsonArray.getJSONObject(i).getString("phone_number"),
                    jsonArray.getJSONObject(i).getString("website"),
                    jsonArray.getJSONObject(i).getString("dbn")
                )
            )
        }
        //Calls the setData() function to initialise the adapter with the api response and display on the screen
        setData()
    }

    //Displaying error message from api as an alert dialog
    fun displayError(errorMessage:String){
        //Handling Empty response from api
        val alertDialog = AlertDialog.Builder(this@MainActivity).create()
        alertDialog.setTitle("Alert")
        alertDialog.setMessage(errorMessage)
        alertDialog.setButton(
            AlertDialog.BUTTON_NEUTRAL, "OK"
        ) { dialog, which -> onBackPressed() }
        alertDialog.show()
    }
}